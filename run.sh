#!/usr/bin/env bash
if [ $# -lt 1 ]
then
	echo "Укажите, какой файл рисовать"
	exit 1
fi

bash start.sh $1

COUNTER=0
MAX=512
while [ ${COUNTER} -lt ${MAX} ] 
do
	echo Рисуем $COUNTER
	bash step.sh
	cp canvas.png ${COUNTER}.png

	COUNTER=$((COUNTER+1))
done

