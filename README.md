Данный набор скриптов генерирует изображение, отталкиваясь от заданного шаблона. Изображение генерируется путем рисования кругов случайного цвета, размера и прозрачности в случайно заданном месте. Затем, если данный круг приблизил изображение к шаблону, то он закрепляется на канве, иначе рисуется другой круг. Затем все повторяется.

__Структура проекта__

1. run.sh - подготавливает изображение (белый лист) и рисует такое количество закрепившихся кругов, какое было указано в данном скрипте в переменной MAX
2. Если вы не хотите сразу рисовать серию кругов, то есть скрипты start.sh (создает белый рисунок такого же размера, как шаблон) и step.sh - добавляет на рисунок 1 круг. По сути run.sh просто выполняет эти 2 скрипта: сначала 1 раз start.sh и затем n-ое количество step
3. Также есть 2 скрипта - diff-image-builder.sh (сравнивает 2 изображения) и draw-on-image.sh (рисует рандомный круг на изображении).

__TODO__

Не писать промежуточные файлы на диск
