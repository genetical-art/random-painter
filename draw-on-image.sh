#!/usr/bin/env bash
if [ $# -lt 2 ]
then
	echo "Нужно указать обрабатываемый файл и файл, в который сохранить результат"
	exit 1
fi
TARGET="$1"
OUTPUT="$2"

R=$((RANDOM % 256))
G=$((RANDOM % 256))
B=$((RANDOM % 256))
A=$((RANDOM % 10001))

W=$(identify "${TARGET}" | sed -ne 's/^.* \([[:digit:]]\+\)x[[:digit:]]\+ .*$/\1/p')
H=$(identify "${TARGET}" | sed -ne 's/^.* [[:digit:]]\+x\([[:digit:]]\+\) .*$/\1/p')

if [ $W -lt $H ]
then
	RAD=$H
else
	RAD=$W
fi

X=$((RANDOM % W))
Y=$((RANDOM % H))
RADIUS=$((RANDOM % RAD))
X2=$((X+RADIUS))
A=$(echo $A | awk '{printf("%f", $1 / 10000)}' | tr , .)
convert "${TARGET}" -fill 'rgba('"$R,$G,$B,$A"')' -draw "circle $X,$Y $X2,$Y" "${OUTPUT}"
