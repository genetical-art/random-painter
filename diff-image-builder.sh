#!/usr/bin/env bash
if [ $# -lt 2 ]
then
	echo Укажите 2 файла для сравнения
	exit 1
fi
if [ ! -f "$1" ]
then
	echo Первого файла не существует
	exit 2
fi
if [ ! -f "$2" ]
then
	echo Второго файла не существует
	exit 3
fi

TARGET_FILE="$1"
SAMPLE_FILE="$2"

compare -metric RMSE "${TARGET_FILE}" "${SAMPLE_FILE}" /dev/null 2>/dev/stdout | awk '{ print $1 }'

