#!/usr/bin/env bash
INITIAL_VAL=$(bash diff-image-builder.sh etalon.png canvas.png)

SUCCESS="-"
while [ $SUCCESS != "+" ]
do
	bash draw-on-image.sh canvas.png result.png
	RESULT_VAL=$(bash diff-image-builder.sh etalon.png result.png)
	SUCCESS=$(echo "" | awk -v A=${INITIAL_VAL} -v B=${RESULT_VAL} '{ if (A > B) {print "+"} else {print "-"}}')
	echo Init: ${INITIAL_VAL}
	echo Res: ${RESULT_VAL}
	echo "-----"
	sleep 1
done
mv result.png canvas.png
