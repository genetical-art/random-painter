#!/usr/bin/env bash
if [ $# -lt 1 ]
then
	echo "Нужно указать эталон"
	exit 1
fi

ETALON="$1"
if [ ! -f "${ETALON}" ]
then
	echo "Файл эталона не существует"
	exit 2
fi

cp -v "${ETALON}" ./etalon.png

W=$(identify "${ETALON}" | sed -ne 's/^.* \([[:digit:]]\+\)x[[:digit:]]\+ .*$/\1/p')
H=$(identify "${ETALON}" | sed -ne 's/^.* [[:digit:]]\+x\([[:digit:]]\+\) .*$/\1/p')

convert -size ${W}x${H} xc:white canvas.png

